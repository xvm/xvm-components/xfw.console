# Changelog

## v11.0.0

* fix compatibility with MT 1.30.0

## v10.0.0

* fix compatibility with WoT 1.18.0

## v1.0.3

* fix version in .json and .xml
* add ability to upload symbols

## v1.0.2

* AMD64 support

## v1.0.1

* WoT 1.7.1 support

## v1.0.0

* first version
